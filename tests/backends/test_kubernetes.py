import json
import tempfile
from pathlib import Path
from typing import Any, Dict, Generator

import pytest
import yaml
from toolforge_weld.kubernetes import K8sClient
from toolforge_weld.kubernetes_config import Kubeconfig, fake_kube_config

from toolsws.backends.kubernetes import (
    KubernetesBackend,
    KubernetesRoutingHandler,
)
from toolsws.tool import Tool
from toolsws.wstypes.ws import WebService

FAKE_IMAGE_CONFIG_DATA = {
    "kind": "ConfigMap",
    "apiVersion": "v1",
    # spec omitted, since it's not really relevant
    "data": {
        "images-v1.yaml": """
jdk17:
  state: stable
  aliases:
    - tf-jdk17
  variants:
    jobs-framework:
      image: docker-registry.tools.wmflabs.org/toolforge-jdk17-sssd-base
    webservice:
      image: docker-registry.tools.wmflabs.org/toolforge-jdk17-sssd-web
      extra:
        wstype: generic
        resources: jdk
node12:
  aliases:
  - tf-node12
  - tf-node12-DEPRECATED
  state: deprecated
  variants:
    jobs-framework:
      image: docker-registry.tools.wmflabs.org/toolforge-node12-sssd-base
    webservice:
      image: docker-registry.tools.wmflabs.org/toolforge-node12-sssd-web
      extra:
        wstype: lighttpd
node16:
  aliases:
  - tf-node16
  state: stable
  variants:
    jobs-framework:
      image: docker-registry.tools.wmflabs.org/toolforge-node16-sssd-base
    webservice:
      image: docker-registry.tools.wmflabs.org/toolforge-node16-sssd-web
      extra:
        wstype: js
php7.3:
  aliases:
  - tf-php73
  - tf-php73-DEPRECATED
  state: deprecated
  variants:
    jobs-framework:
      image: docker-registry.tools.wmflabs.org/toolforge-php73-sssd-base
    webservice:
      image: docker-registry.tools.wmflabs.org/toolforge-php73-sssd-web
      extra:
        wstype: lighttpd
php7.4:
  aliases:
  - tf-php74
  state: stable
  variants:
    jobs-framework:
      image: docker-registry.tools.wmflabs.org/toolforge-php74-sssd-base
    webservice:
      image: docker-registry.tools.wmflabs.org/toolforge-php74-sssd-web
      extra:
        wstype: lighttpd
""",
    },
}


@pytest.fixture
def fake_k8s_client() -> K8sClient:
    return K8sClient(
        kubeconfig=fake_kube_config(),
        user_agent="webservice",
    )


@pytest.fixture
def patch_k8s_client(monkeypatch):
    def mock_get_object(self, *args, **kwargs):
        # this is a hack
        if args == ("configmaps", "image-config") and kwargs == {
            "namespace": "tf-public"
        }:
            return FAKE_IMAGE_CONFIG_DATA

        raise Exception("not expected to reach this point")

    monkeypatch.setattr(
        Kubeconfig, "load", lambda *args, **kwargs: fake_kube_config()
    )
    monkeypatch.setattr(K8sClient, "get_object", mock_get_object)


@pytest.fixture()
def patch_k8s_backend_get_types_no_lrucache():
    KubernetesBackend.get_types.cache_clear()


@pytest.fixture
def fake_tool(monkeypatch) -> Generator[Tool, None, None]:
    # patch so it doesn't need to access the file system (which fails on CI)
    Tool._PROJECT = "tools"
    kube_config_raw = yaml.safe_dump(
        {
            "contexts": [
                {
                    "name": "toolforge",
                    "context": {"namespace": "tool-test"},
                }
            ]
        }
    )
    monkeypatch.setattr(
        "toolsws.tool.Tool._load_kubeconfig",
        lambda _: kube_config_raw,
    )
    monkeypatch.setattr(
        "toolsws.backends.kubernetes.Tool._load_kubeconfig",
        lambda _: kube_config_raw,
    )
    with tempfile.TemporaryDirectory() as tmpdir:
        yield Tool("test", 52503, tmpdir)


def test_KubernetesRoutingHandler_public_domain_default(
    fake_k8s_client: K8sClient,
    fake_tool: Tool,
):
    handler = KubernetesRoutingHandler(
        api=fake_k8s_client,
        tool=fake_tool,
        namespace=f"tool-{fake_tool.name}",
        webservice_config={},
    )

    assert (
        handler._get_ingress_subdomain()["spec"]["rules"][0]["host"]
        == "test.toolforge.org"
    )


def test_KubernetesRoutingHandler_public_domain_config(
    fake_k8s_client: K8sClient,
    fake_tool: Tool,
):
    handler = KubernetesRoutingHandler(
        api=fake_k8s_client,
        tool=fake_tool,
        namespace=f"tool-{fake_tool.name}",
        webservice_config={"public_domain": "example.com"},
    )

    assert (
        handler._get_ingress_subdomain()["spec"]["rules"][0]["host"]
        == "test.example.com"
    )


def test_KubernetesBackend_get_types(
    patch_k8s_client,
    patch_k8s_backend_get_types_no_lrucache,
    fake_tool,
):
    types = KubernetesBackend.get_types()
    assert type(types) is dict

    for name, data in types.items():
        if name == "buildservice":
            continue

        assert type(name) is str
        assert type(data) is dict

        assert issubclass(data["cls"], WebService)
        assert type(data["image"]) is str
        assert "limits" in data["resources"]


@pytest.mark.parametrize(
    "wstype,given,expected",
    [
        ["php7.4", {}, KubernetesBackend.DEFAULT_RESOURCES["default"]],
        ["jdk17", {}, KubernetesBackend.DEFAULT_RESOURCES["jdk"]],
        [
            "php7.4",
            {"cpu": "1"},
            {
                "limits": {
                    "memory": "512Mi",
                    "cpu": "1",
                },
                "requests": {
                    "memory": "256Mi",
                    "cpu": "0.5",
                },
            },
        ],
    ],
)
def test_KubernetesBackend_parse_resources(
    patch_k8s_client,
    patch_k8s_backend_get_types_no_lrucache,
    fake_tool: Tool,
    wstype: str,
    given: dict,
    expected: dict,
):
    backend = KubernetesBackend(
        fake_tool,
        wstype,
        mem=given.get("mem", None),
        cpu=given.get("cpu", None),
        webservice_config={},
    )

    assert type(backend.container_resources) is dict
    assert backend.container_resources == expected


@pytest.fixture
def deployment1() -> Dict[str, Any]:
    file = Path(__file__).parent.parent / "fixtures" / "deployment1.json"
    with open(file, "r") as f:
        return json.load(f)


# TODO: extend this to cover more code branches
@pytest.mark.parametrize(
    ["wstype", "expected_deployment"],
    [
        ["php7.4", "deployment1"],
    ],
)
def test_KubernetesBackend_get_deployment(
    request,
    patch_k8s_client,
    patch_k8s_backend_get_types_no_lrucache,
    fake_tool: Tool,
    wstype: str,
    expected_deployment: Dict[str, Any],
) -> None:
    backend = KubernetesBackend(
        fake_tool,
        wstype,
        webservice_config={},
    )
    assert backend._get_deployment(
        started_at="sometime"
    ) == request.getfixturevalue(expected_deployment)
