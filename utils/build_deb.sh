#!/bin/bash

set -o pipefail
set -o errexit


if [[ "${1}" == "--no-cache" ]]; then
    no_cache=(--no-cache)
    shift
fi

if [[ "${1}" == "bookworm" ]]; then
    distro="${1}"
    shift
else
    distro="bookworm"
fi

DOCKER="docker"
extra_options=("--volume=$PWD:/src:rw")
if command -v podman >/dev/null; then
    DOCKER="podman"
    extra_options=(
        "--volume=$PWD:/src:rw,z"
        "--userns=keep-id"
    )
fi

if [[ $distro == "buster" ]]; then
    echo "We can't build on buster for now as it needs poetry that needs python>3.7"
    exit 1
fi


$DOCKER build "utils/debuilder-${distro}" "${no_cache[@]}" -t "debuilder-${distro}:latest"
# cleanup old packages/build artifacts
rm -rf build .pybuild
# we need root privileges to install packages
$DOCKER run --user 0 --rm "${extra_options[@]}" "debuilder-${distro}:latest"
