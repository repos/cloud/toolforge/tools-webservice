#!/bin/bash

set -x
set -o nounset
set -o pipefail
set -o errexit

export DEST_DISTRO=${DISTRO}

cd /src
echo "Updating changelog..."
new_version="${1:+--new-version="$1"}"
# shellcheck disable=SC2086
# Double quote to prevent globbing and word splitting.
EDITOR=true gbp dch \
    --release \
    $new_version

cur_version="$(dpkg-parsechangelog -S version)"
sed -i "s/version=.*/version=\"$cur_version\",/" setup.py

echo "Now you can send this patch for review." \
    "A tag named 'debian/$cur_version' has been created. Remember to push it when publishing the package."
