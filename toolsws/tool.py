import datetime
import errno
import os
from pathlib import Path
from typing import Union

import yaml


class Tool:
    _PROJECT: Union[str, None] = None

    @staticmethod
    def get_current_project():
        if Tool._PROJECT is None:
            with open("/etc/wmcs-project", "r") as _projectfile:
                Tool._PROJECT = _projectfile.read().strip()
        return Tool._PROJECT

    @staticmethod
    def get_prefix():
        return Tool.get_current_project() + "."

    MANIFEST_VERSION = 4

    class InvalidToolException(Exception):
        pass

    def __init__(self, name, uid, home):
        self.name = name
        self.uid = uid
        self.home = home

    def get_homedir_subpath(self, path):
        return os.path.join(self.home, path)

    @property
    def manifest(self):
        """
        Return a dict with data from service manifest for current tool
        instance.

        If no service.manifest file is found, returns an empty dict
        """
        if not hasattr(self, "_manifest"):
            try:
                with open(self.get_homedir_subpath("service.manifest")) as f:
                    self._manifest = yaml.safe_load(f)
                if self._manifest is None:
                    self._manifest = {}
            except IOError as e:
                if e.errno == errno.ENOENT:
                    self._manifest = {}
                else:
                    raise
        return self._manifest

    def save_manifest(self):
        """
        Saves modified manifest file to service.manifest

        Never write to service.manifest without an action directly initiated
        by a user (like running a commanad on the commandline). If a race
        happens, whoever wins the os.rename race wins.
        """
        tilde_file_path = self.get_homedir_subpath("service.manifest~")
        tilde_file_fd = os.open(
            tilde_file_path, os.O_CREAT | os.O_EXCL | os.O_WRONLY, 0o644
        )
        tilde_file = os.fdopen(tilde_file_fd, "w")
        self.manifest["version"] = Tool.MANIFEST_VERSION
        try:
            tilde_file.write(
                "# This file is used by Toolforge infrastructure.\n"
                "# Please do not edit manually at this time.\n"
                "# {:%c}\n".format(datetime.datetime.now())
            )
            yaml.safe_dump(
                self._manifest, tilde_file, default_flow_style=False
            )
        finally:
            tilde_file.close()

        # We leave behind tilda_file_path if this rename fails for some
        # reason, and then manual intervention is needed, but that seems
        # appropriate
        os.rename(
            tilde_file_path, self.get_homedir_subpath("service.manifest")
        )

    @classmethod
    def from_currentuser(cls):
        """
        Create a Tool instance for the current running user
        """
        return Tool.from_kubeconfig()

    @staticmethod
    def _load_kubeconfig(kube_path: Path) -> str:
        """For easy mocking."""
        return kube_path.read_text(encoding="utf8")

    @classmethod
    def from_kubeconfig(cls):
        """
        Creates a tool instance for the current configured kubeconfig.

        TODO: we will have to add eventually a flag for it, though hopefully will happen after this merges into
              the jobs-api.
        """
        uid = os.geteuid()

        # we assume that if running inside a container, TOOL_DATA_DIR is set, and outside in the bastion, the current
        # user home matches the tool home inside the container
        home = str(Path(os.environ.get("TOOL_DATA_DIR", "~")).expanduser())

        kubeconfig = Path(home) / ".kube/config"

        # avoid needing kubectl, though it's currently required for shell, not using it here allows running
        # start/stop/restart without it, the ones mostly used in non-interactive jobs (likely cronjobs)
        try:
            raw_config = cls._load_kubeconfig(kubeconfig)
        except Exception as error:
            raise Tool.InvalidToolException(
                "Unable to load the tool's kubeconfig"
            ) from error
        loaded_config = yaml.safe_load(raw_config)
        try:
            namespace = loaded_config["contexts"][0]["context"]["namespace"]
        except KeyError:
            raise Tool.InvalidToolException(
                "Malformed kubeconfig found, missing the namespace in the default context"
            )

        name = namespace.split("-", 1)[-1]

        return cls(name=name, uid=uid, home=home)
