from setuptools import find_packages, setup

setup(
    name="toolforge-webservice",
    version="0.103.15",
    author="Yuvi Panda",
    author_email="yuvipanda@gmail.com",
    license="Apache2",
    packages=find_packages(),
    entry_points={
        "console_scripts": [
            "toolforge-webservice = toolsws.cli.webservice:main",
            "webservice = toolsws.cli.webservice:main",
        ]
    },
    description="Infrastructure for running webservices on Toolforge",
    install_requires=["PyYAML", "requests", "toolforge-weld>=1.1.1"],
)
