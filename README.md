# tools-webservice

This Debian package will deploy the `toolforge webservice` command line
interface which enables Toolforge users to host and interact with webservices in
Toolforge.

It is typically installed in Toolforge bastions servers.

Documentation about how to use the `webservice` utility can be found at:

https://wikitech.wikimedia.org/wiki/Help:Toolforge/Web

## Deploying

See https://wikitech.wikimedia.org/wiki/Portal:Toolforge/Admin/Packaging

## Development environment

You can use
[lima-kilo](https://gitlab.wikimedia.org/repos/cloud/toolforge/lima-kilo) like
the rest of the components/clis.
